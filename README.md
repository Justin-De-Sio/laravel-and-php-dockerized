# Laravel & PHP Dockerized

## Description

This Docker Compose configuration sets up a Laravel application stack, which includes the following services:

- Nginx (Web server)
- PHP (Application runtime)
- MySQL (Database)
- Composer (PHP Dependency Manager)
- Artisan (Laravel's Command-line tool)
- NPM (Node Package Manager)

## Prerequisites

Before you begin, make sure you have installed the following:

- [Docker](https://www.docker.com/products/docker-desktop)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Directory Structure

```plaintext
.
├── dockerfiles/          # Dockerfiles for PHP, Composer, and NPM
├── env/                  # Environment variables for MySQL
├── nginx/                # Nginx configuration files
├── src/                  # Source code of the application
└── docker-compose.yml    # Docker Compose configuration file
```

## Getting Started

### Step 1: Clone the Repository

Open your terminal and execute:

```bash
git clone <repository_url>
```

### Step 2: Navigate to the Project Directory

Change directory to the project folder:

```bash
cd <project_directory>
```

### Step 3: Create a Laravel Project

If you don't have an existing Laravel project, you can create a new one within your `src` folder. Use the Composer service defined in your Docker Compose configuration to do this:

```bash
docker compose run --rm composer create-project --prefer-dist laravel/laravel:^8.2 .
```

### Step 4: Environment Variables for MySQL

To configure MySQL, you'll need to create a `mysql.env` file within the `./env/` directory. An example configuration—`mysql.env.example`—is already provided for your reference. Copy this example file, rename it to `mysql.env`, and fill in your specific settings.

### Step 5: Start Services

Start the MySQL, PHP, and Nginx services. This will run these services in the background (`-d` flag):

```bash
docker compose up mysql php server -d
```

### Step 6: Database Migration

After the services are up and running, you can migrate your database tables using Laravel's Artisan tool. This will create the necessary tables in your MySQL database:

```bash
docker compose run --rm artisan migrate
```

### Optional: Seed Database (If applicable)

If your Laravel project includes database seeders, execute:

```bash
docker compose run --rm artisan db:seed
```

### Step 7: Access the Application

At this point, your Laravel application should be accessible at `http://localhost:8000`.

## Stopping Services

When you're done, you can stop all services by executing:

```bash
docker compose down
```